# 基本情報

トランスポート層は TCP を使用します。ポート番号は主に7144番が使用されますが、任意の番号を使用できます。

また、特記しない限りデータの符号化は以下の形式で行われます。

- 平文を使用する
- 数値は Little Endian を使用する
- 文字列は null 終端を含む

# pcp と Atom

PeerCast の主要な通信は独自のバイナリフォーマットのストリームで行います。実装の雰囲気的にはこのストリームのことを pcp と言うっぽいです。
pcp は複数の可変長の塊の集合です。この可変長の塊の一つ一つを Atom と呼びます。 Atom のひと塊は以下に示すバイナリデータです。

バイト長 | フォーマット | 内容
---- | ---- | ----
4 | ASCII文字列 | 識別子 (4文字未満の場合は \0 埋め)
4 | uint32 | データ部の長さ、または子 Atom の数
任意長 | 任意のデータ | (子 Atom のみ)データ部

「データ部の長さ、または子 Atom の数」の最上位 1 bit により、この Atom が親 Atom なのか子 Atom なのかが決まります。
最上位 1bit が 0 の時は子 Atom です。残り 31 bits は任意のデータのバイト長です。
最上位 1bit が 1 の時は親 Atom です。残り 31 bits は子 Atom の数です。データ部は付きません。

例1)
```
|p|c|p|\n|0x04|0x00|0x00|0x00|0x01|0x00|0x00|0x00|
```

これは以下のように解釈します。

バイト長 | 内容
---- | ----
4 | "pcp\n"
4 | (子) データ部 4 バイト
4 | 0x00000001

例2)
```
|h|e|l|o|0x01|0x00|0x00|0x80|s|i|d|\0|0x10|0x00|0x00|0x00|(16バイトのバイナリデータ)|
```

これは以下のように解釈します。

バイト長 | 内容
---- | ----
4 | "helo"
4 | (親) 子 1 個
4 | "sid"
4 | (子) データ部 16 バイト
16 | (16バイトのバイナリデータ)

ただし、 TCP 接続を確立した後に必ずしもこのフォーマットで通信するわけではありません。

# pcp ハンドシェイク

PeerCast は面倒なことに、通信の目的によってヘッダーのパターンが異なります。いずれにしろなんやかんやして準備が整ったら pcp ハンドシェイクを行います。
ヘッダーについて説明する前に、共通の処理である pcp ハンドシェイクを説明します。

pcp ハンドシェイクの手順は以下の通りです。接続をしに行く側をクライアント、接続を受ける側をサーバーと呼称します。

1. クライアントはサーバーへ **helo** Atom を送る
2. 受け取ったサーバーは、もし ping がセットされている場合、そのポート番号で通信可能か確認する
    1. サーバーはクライアントへ ping のポート番号に対し TCP 接続を開始し、 **pcp\n** Atom と **helo** Atom を送る
    2. クライアントは **oleh** Atom を送る
        - この時、 1. で送った helo の sid と同じ値を oleh の sid に使用する
    3. サーバーは、 oleh の sid と予め helo で受け取っていた sid が一致するか確認し、一致しているなら通信成功とする
    4. サーバーは **quit** Atom を送り、通信を終了する
3. サーバーはクライアントへ **oleh** Atom を送る
4. サーバーはもし root モードなら **root** Atom を送信する

以上まででお互いに通信内容に問題なければハンドシェイク成功です。もし途中で気に食わないことがあれば **quit** Atom を送って通信を終了します。

# チャンネル掲載

ここからは目的別に通信方法を説明します。まずはチャンネル掲載について説明します。

チャンネルリストを提供する PeerCast は、配信を行う PeerCast からチャンネル情報の提供を受け付ける必要があります。


1. クライアントはサーバーへ **pcp\n** Atom を送る
2. クライアントからサーバーへ pcp ハンドシェイクを行う
3. クライアントはサーバーへ **bcst** Atom を送る
4. サーバーは **bcst** Atom に乗せた配信情報をチャンネルリストに掲載する
5. 配信が終了したら、クライアントはサーバーへ **quit** Atom を送る

# リレー要求

リレー要求を行う場合、 http 的なリクエストヘッダを投げつけ合い、途中から Atom の送受信に切り替えます。多分 http って言ったらおおよそ怒られるような挙動をします。 Upgrade ヘッダー使ってたら http の仕様に沿うことができたと思うんですが、まあ WebSocket が発明されるずっと前のプロトコルなので致し方なし。

改行文字は送り側の OS によって CRLF だったり LF だったりします。ちなみに http の仕様だと CRLF しか許可されていません。 ( https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html )
streamPos はよくわからんのでそのうち調べる。

1. クライアントはサーバーへ以下のプレーンテキストを送る
```
GET /channel/【Channel id (16進数)】 HTTP/1.0【改行】
x-peercast-pos: 【streamPos(?)】【改行】
x-peercast-pcp: 【多分 pcp のバージョン】【改行】
【改行】
```
2. サーバーは指定の id のチャンネルを持っているか確認し、持っていない、持っているがリレーできない、持っているのいずれかによりそれぞれ異なる応答をする

## 持っていない場合

- サーバーはサーバーへ以下のプレーンテキストを送り、切断する
```
HTTP/1.0 404 Not Found【改行】
【改行】
```

## 持っているがリレーできない場合

1. サーバーはクライアントへ以下のプレーンテキストを送る
```
HTTP/1.0 503 Service Unavailable【改行】
Content-Type: application/x-peercast-pcp【改行】
【改行】
```
2. クライアントからサーバーへ pcp ハンドシェイクを行う
3. サーバーはクライアントへ **host** 親子 Atom を複数個送る
4. サーバーはクライアントへ **quit** Atom を送り、切断する

後はクライアントは送られてきた host に対してリレー要求を開始すればよいです。

## 持っている場合

1. サーバーはクライアントへ以下のプレーンテキストを送る
```
HTTP/1.0 200 OK【改行】
Server: PeerCast/0.1218【改行】
Accept-Ranges: none【改行】
x-audiocast-name: 【チャンネル名】【改行】
x-audiocast-bitrate: 【ビットレート】【改行】
x-audiocast-genre: 【Genre】【改行】
x-audiocast-description: 【Description】【改行】
x-audiocast-url: 【url】【改行】
x-peercast-channelid: 【Channel id】【改行】
x-peercast-pos: 【streamPos】【改行】
Content-Type: application/x-peercast-pcp【改行】
【改行】
```
2. クライアントからサーバーへ pcp ハンドシェイクを行う
3. サーバーはクライアントへ **ok** Atom を送る
4. サーバーはクライアントへ **chan** 親子 Atom に配信のデータを詰めて送るらしい
5. 時折 **bcst** 親子 Atom をやり取りするらしいけど、もう本家の実装見ても全然わからん。検証したりPeCaSt実装見るとかしないと無理。

# push リレー

PeerCast は実は NAT やファイヤーウォールなどによりサーバーになれない環境 (いわゆるポート0) でもリレーをするための機能が存在します。プロトコルは Gnutella に由来します。

TCP 接続を確立したら最初に以下のプレーンテキストを送るのが特徴です。
```
GIV /【Channel id】【改行】
```

多分上位のピアが仲介役になって、ポト0ピアにGIVの送信を指示し、新規ピアにGIVの受信を指示するんだと思います。詳しくはそのうち調べる。

# 付録. Atom の構造

| 識別子 | 型 | 内容  | 例
| ----- | -- | ---- | ----
| pcp\n | int32 | プロトコルバージョン | 0x00000001

| 識別子 | 型 | 内容  | 例
| ----- | -- | ---- | ----
| quit | int32 | 通信終了理由 | 1000

| 識別子 |    型 | 内容       | 例
| ----- | ----- | --------- | ----
| ok    | int32 | (値未使用) | 0

| 識別子 | 識別子 | 型 | 内容  | 例
| ----- | ----- | -- | ---- | ----
| helo | | parent | ハンドシェイク
| | sid  | binary (16 bytes) | 送信元セッション id |
| | agnt | string | [Option] 送信元ユーザーエージェント | PeerCast/0.1218
| | ver  | int32  | [Option] 送信元バージョン | 1218
| | port | int16 | [Option] 待受ポート番号 | 7144
| | ping | int16 | [Option] 待受ポート番号 (開放確認を行う) | 7144
| | bcid | binary (16 bytes) | [Option] 配信するチャンネル id
| | osty | int32 | [Option] [Unused] OS Type (?)

| 識別子 | 識別子 | 型 | 内容  | 例
| ----- | ----- | -- | ---- | ----
| oleh | | parent | ハンドシェイク
| | sid  | binary (16 bytes) | 送信元セッション id |
| | agnt | string | [Option] 送信元ユーザーエージェント | PeerCast/0.1218
| | ver  | int32  | [Option] 送信元バージョン | 1218
| | rip  | uint32<br>[Unofficial] uint32 or uint128 | [Option] oleh 側から見た helo 側の リモート IP (IPv4) [Unofficial] IPv6 を指定できる
| | port | uint16 | [Option] oleh 側から見た helo 側の ポート番号
| | dis  | int32  | [Option] disable (?)

| 識別子 | 識別子 | 型 | 内容  | 例
| ----- | ----- | -- | ---- | ----
| root | | parent | 
| | uint | int32  | ホスト更新間隔(?)(秒) | 120
| | url  | string | [Option] ダウンロード URL の **http(コロン)//www.peercast.org/ 以下のパス** | download.php
| | chkv | int32  | [Option] 最新版のバージョン | 1218
| | next | int32  | [Option] 次に root Atom を送るまでの時間(?)(秒) | 120
| | asci | string | [Option] メッセージ(?) |

|識別子|識別子|識別子| 型 | 内容  | 例
|-----|-----|-----| -- | ---- | ----
| chan | | | parent |
| | id   | | binary (16 bytes) | channel id | 
| | bcid | | binary (16 bytes) | [Option] broadcast id |
| | info | | parent | [Option]
| | | name | string | チャンネル名
| | | bitr | int32  | bitrte
| | | gnre | string | genre
| | | url  | string | url
| | | desc | string | description
| | | cmnt | string | comment
| | | type | string | type
| | | styp | string | [Unofficial] MIME Type
| | | sext | string | [Unofficial] "." で始まる拡張子
| | trck | | parent | [Option] track
| | | titl | string | title
| | | crea | string | creator (artist)
| | | url  | string | url
| | | albm | string | album
| | | gnre | string | [Unofficial] genre
| | pkt  | | parent | [Option] packet
| | | type | ASCII (4 bytes) | "head" or "data"
| | | pos  | int32  | pos ?
| | | cont | int8   | [Unofficial] 継続パケット | 1
| | | data | binary | データ

| 識別子 | 識別子 | 型 | 内容  | 例
| ----- | ----- | -- | ---- | ----
| host | | parent |
| | cid  | binary (16 bytes) | [Option] channel id
| | id   | binary (16 bytes) | session id
| | ip   | int32  | ip
| | port | uint16  | port
| | ip   | int32  | 2つ目の ip
| | port | uint16  | 2つ目の port
| | numl | int32  | listener
| | numr | int32  | relay
| | uptm | int32  | uptime
| | ver  | int32  | version
| | vevp | int32  | [Unofficial] version VP
| | vexp | ASCII (2 bytes) | [Unofficial] version ex prefix
| | vexn | int16  | [Unofficial] version ex number
| | flg1 | int8   | [Flag] flags1
| | oldp | int32  | oldest pos
| | newp | int32  | newest pos
| | upip | int32  | [Option] uphost ip
| | uppt | int32  | [Option] uphost port
| | uphp | int32  | [Option] uphost hops

| 識別子 | 識別子 | 識別子 | 識別子 | 型 | 内容  | 例
| ----- | ----- | ----- | ----- | -- | ---- | ----
| bcst | | | | parent | 
| | ttl  | | | int8   | ttl? (7または11で始まり、リレーされるごとに-1される) | 7
| | hops | | | int8   | 中継数 (配信元なら0) | 0
| | from | | | binary (16 bytes) | セッション id ?
| | dest | | | binary (16 bytes) | [Option] セッション id ?
| | grp  | | | int8   | group? (0x01: ROOT, 0x02: TRACKERS, 0x04: RELAYS) | 0xff
| | cid  | | | binary (16 bytes) | [Option] channel id
| | vers | | | int32  | 送信元バージョン | 1218
| | vrvp | | | int32  | [Unofficial] 送り元VP版バージョン | 27
| | vexp | | | ASCII (2 bytes) | [Unofficial] 送り元拡張バージョンプレフィックス | YT
| | vexn | | | int16  | [Unofficial] 送り元拡張バージョン番号 | 30
| | chan | | | parent | [Option] (前述のものと同じ)
| | root | | | parent | [Option] (前述のものと同じ)
| | host | | | parent | [Option] (前述のものと同じ)
| | push | | | parent | [Option]
| | | ip   | | int32  | ip
| | | port | | int8   | port
| | | cid  | | binary (16 bytes) | channel id

```
flags1
0b01111111
   |||||||
   ||||||+-TRACKER
   |||||+--RELAY   リレー可能かどうか
   ||||+---DIRECT
   |||+----PUSH
   ||+-----RECV
   |+------CIN
   +-------PRIVATE
```
