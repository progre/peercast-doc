```mermaid

classDiagram

global *-- ChanMgr
global *-- ServMgr

VariableWriter <|-- ChanMgr
ChanMgr *-- Channel

VariableWriter <|-- Channel
Channel *-- ClientSocket
Channel *-- ThreadInfo
ThreadInfo o-- Channel

Stream <|-- ClientSocket
ClientSocket *-- Host
ClientSocket ..> netinet

HTTPPushSource *-- ClientSocket

Servent o-- Servent
Servent *-- PCPStream
Servent *-- ClientSocket
Servent *-- ThreadInfo

ChannelStream <|-- PCPStream

ServMgr *-- Servent
ServMgr *-- ThreadInfo

```
